#/bin/make
#@ DevPi Docker Image

DEVPI_NAME ?= "DevPi Server"
DEVPI_VERSION ?= "v0.1.0"
DEVPI_DESCRIPTION ?= "Display a help console message for Makefiles."
DEVPI_ROOT ?= $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
-include .env
export

%: %-devpi
	@true

check-%:
	@test -n "${$(*)}"

.DEFAULT_GOAL := help
.PHONY: help #: testing
help:
	@cd ${DEVPI_ROOT} && awk 'BEGIN {FS = " ?#?: "; print ""${DEVPI_NAME}" "${DEVPI_VERSION}"\n"${DEVPI_DESCRIPTION}"\n\nUsage: make \033[36m<command>\033[0m\n\nCommands:"} /^.PHONY: ?[a-zA-Z_-]/ { printf "  \033[36m%-10s\033[0m %s\n", $$2, $$3 }' $(MAKEFILE_LIST)

.PHONY: init-devpi #: Login into Docker
init-devpi: check-DOCKER_REGISTRY check-DOCKER_USERNAME check-DOCKER_PASSWORD
	@cd ${DEVPI_ROOT} && \
	[[ -f .env ]] || cp .env.example .env
# 	@docker login ${DOCKER_REGISTRY} -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD} 2>/dev/null

.PHONY: build-devpi #: Build Docker image
build-devpi: check-DOCKER_IMAGE check-DOCKER_VERSION init-devpi 
	@cd ${DEVPI_ROOT}/${IMAGE}/images && \
	docker-compose build ${CONTAINER}

.PHONY: release-devpi
release-devpi: init-devpi #: Push Docker image to registry 
	@cd ${DEVPI_ROOT}/${IMAGE}/images && \
	docker-compose push ${CONTAINER}

.PHONY: pull-devpi #: Pull Docker image from registry
pull-devpi: init-devpi
	@cd ${DEVPI_ROOT}/${IMAGE}/examples && \
	docker-compose pull ${CONTAINER}

.PHONY: run-devpi
run-devpi: init-devpi
	@cd ${DEVPI_ROOT}/${IMAGE}/examples && \
	docker-compose up -d ${CONTAINER}

.PHONY: debug-devpi
debug-devpi: check-CONTAINER init-devpi
	@cd ${DEVPI_ROOT}/${IMAGE}/examples && \
	docker-compose run --service-ports ${CONTAINER} bash

.PHONY: status-devpi
status-devpi: init-devpi
	@cd ${DEVPI_ROOT}/${IMAGE}/examples && \
	docker-compose logs --follow ${CONTAINER}

.PHONY: stop-devpi
stop-devpi: init-devpi
	@cd ${DEVPI_ROOT}/${IMAGE}/examples && \
	docker-compose stop ${CONTAINER}

.PHONY: clean-devpi
clean-devpi: stop-devpi
	@cd ${DEVPI_ROOT}/${IMAGE}/examples && \
	docker-compose rm ${CONTAINER} && \
	trash ${DEVPI_ROOT}/${IMAGE}/instances

.PHONY: open-devpi
open-devpi: check-DEVPI_SERVER_HOST check-DEVPI_SERVER_PORT init-devpi
	@open http://${DEVPI_SERVER_HOST}:${DEVPI_SERVER_PORT}

#- Include common scripts for docker
%:
	@if [[ -f "../scripts-docker/$(*).sh" ]]; then sh "../scripts-docker/$(*).sh"; fi

