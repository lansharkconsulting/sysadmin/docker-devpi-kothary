#!/usr/bin/env bash
set -e

if [[ -f $DEVPI_SERVERDIR/.serverversion ]]; then
	initialize=yes
	echo "ENTRYPOINT: Initializing server root $DEVPI_SERVER_ROOT"
	devpi-server --init --serverdir "$DEVPI_SERVER_ROOT"
fi

# Properly shutdown devpi server
shutdown() {
    devpi-server --stop  # Kill server
    kill -SIGTERM $TAIL_PID  # Kill log tailing
}

trap shutdown SIGTERM SIGINT

# Heroku Deployment Support
DEVPI_PORT=3000

# Need $DEVPI_SERVERDIR
mkdir -p {$DEVPI_SERVERDIR $DEVPI_CLIENTDIR}
devpi-server --start --host 0.0.0.0 --port $DEVPI_PORT --serverdir $DEVPI_SERVER_ROOT --theme $DEVPI_THEME

DEVPI_LOGS=$DEVPI_SERVERDIR/.xproc/devpi-server/xprocess.log

devpi use http://localhost:$DEVPI_PORT
if [[ $initialize = yes ]]; then
  # Set root password
  devpi login root --password=''
  devpi user -m root password="${DEVPI_PASSWORD}"
  # devpi index -y -c public pypi_whitelist='*'
fi
# Authenticate for later commands
devpi login root --password="${DEVPI_PASSWORD}"

tail -f $DEVPI_LOGS &
TAIL_PID=$!

# Wait until tail is killed
wait $TAIL_PID

# Set proper exit code
wait $DEVPI_PID
EXIT_STATUS=$?